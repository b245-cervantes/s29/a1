// find user with letter s in their firstname or
// d in their last name.

db.users.find({
	$or: [

           {
           	firstName:{$regex:"s",$options:"$i"},
           	},
           	{
           	lastName:{$regex:"d",$options:"$i"}
           	},
   		]
},
	{
		   firstName:1,
		   lastName:1,
		    _id:0
	       }
)


//find users who are from HR department and their age is greater than or equal to 70


db.users.find({
	$and: [
           
           {
           	department: "HR"
           },
           {
           	age: {$gte:70}
           }


		]
})

//find users with letter e oin their first name and has an age of less than or equal to 30

db.users.find({
	$and: [
            {
            	firstName:{$regex:"e",$options:"i"}
            },
            {
            	age:{$lte:30}
            }

		  ]
})